## Getting started Dev Stuff
Install node modules npm install
node server.js



## Dokku GraphQL Deployment Setup
Unlisted commands can be found at http://dokku.viewdocs.io/dokku/deployment/application-management/. "Defaults" are suggested values for production deployment.
* Create the app on the dokku machine `dokku apps:create <app-name>` default is `scraper-server`
* Add a domain `dokku domains:add <app-name> <domain>` default domains are `www.fishhop.com` and `fishhop.com`
  * You may also have to delete the default domain in order to get letsencrypt to work properly. It will fail if there is not a proper FQDN setup or if the apps domains contains a non-FQDN.
* Setup letsecnrpt email `dokku config:set --no-restart <app-name> DOKKU_LETSENCRYPT_EMAIL=<email>` default email is `admin@fishhop.com`
* Setup letsencrypt `dokku letsencrypt <app-name>`

## Dokku Deploy to Existing Setup
* First you need to add a host to your ssh config
  * You will also need to find out what the values for hostname, host, port should be and you will also need to get the private key id file. We have the file stored in our documentation. Below is an emapl host.

~~~~
Host fishhop-dokku01-git
  HostName dokku01.fishhop.com
  User dokku
  Port 22
  IdentityFile ~/.ssh/fishhopadmin
  IdentitiesOnly yes
~~~~

* Add git remote `git remote add <remoteName> dokku@<host-from-ssh-config>:<app-name>`
* Deploy just do `git push <remoteName>`

## Other Env variables
`AUTH_APP_ID` = ID that is stored in the API server database
`AUTH_SECRET` = Secret used to generate API token, it should match what the API server uses

## TODO:
....

## Notes
* NPM is currently used over Yarn