const Apollo = require('@apollo/client/core')
const jwt = require('jsonwebtoken');
const fetch = require('cross-fetch');

const { ApolloClient, HttpLink, ApolloLink, InMemoryCache, concat } = Apollo;

const appAuthId = process.env.AUTH_APP_ID;
const apiAuthSecret = process.env.AUTH_SECRET;
const apiTokenData = { type: 'api', appId: appAuthId };
let apiAuthToken = jwt.sign({ exp: Math.floor(Date.now() / 1000) + (60 * 60), data: apiTokenData }, apiAuthSecret);
apiAuthToken = `apiToken-${apiAuthToken}`

const authMiddleware = new ApolloLink((operation, forward) => {
  // add the authorization to the headers
  operation.setContext({
    headers: {
      authorization: apiAuthToken,
    }
  });
  return forward(operation);
});

function getClient() {
  const httpLink = new HttpLink({
    uri: process.env.API_URI,
    fetch

  });
  return new ApolloClient({
    link: concat(authMiddleware, httpLink),
    cache: new InMemoryCache(),
    defaultOptions: {
      query: {
        fetchPolicy: 'no-cache'
      }
    }
  });
}
exports.default = getClient;