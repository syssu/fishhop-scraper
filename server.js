require('dotenv').config()
const express = require("express");
const schedule = require("node-schedule");
const { exec } = require("child_process");
const basicAuth = require('express-basic-auth')

const app = express();
const port = process.env.PORT ? process.env.PORT : 4000;

let output = "All your kittens belong to me!";

app.use(basicAuth({
  // TODO: Move this over to env variable
  users: { 'admin': '1635957027' }
}))

app.get("/log", (req, res) => res.send(`<pre id="log">${output}</pre>`));
app.get("/execute", (req, res) => {
  execute();
  res.send(`<pre id="log">${output}</pre>`);
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));

schedule.scheduleJob("0 1-23/2 * * *", async function() {
  execute(); 
});

function execute() {
  exec(
    `./node_modules/.bin/wdio --mochaOpts.grep ca-sacramento-get-report wdio.conf.js`,
    (err, stdout, stderr) => {
      output = stdout;
    }
  );
}
