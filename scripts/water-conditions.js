const Apollo = require("@apollo/client/core");
const { gql } = Apollo;

function getTimeStamp() {
  const newDate = new Date(Date.now());
  return `${newDate.toTimeString()} ${newDate.toTimeString()}`;
}

describe("ca-sacramento-get-report", function () {
  it("should", async function () {
    
    console.log(`${getTimeStamp()}: Starting update of Sacramento River`);
    const client = global.apiClient;

    const SPOTS_QUERY = gql`
      query SPOT_QUERY($searchTerm: String!) {
        searchSpots(searchTerm: $searchTerm) {
          id
          key
        }
      }
    `;

    const UPDATE_SPOT_MUTATION = gql`
      mutation UPDATE_SPOT_MUTATION($spotId: String!, $data: String!) {
        updateSpot(spotId: $spotId, data: $data) {
          id
          key
          name
        }
      }
    `;

    var element = null;
    // go to
    await browser.url("https://www.theflyshop.com/streamreport.html");

    // expand river reports
    element = "#accordion1 > div:nth-child(1) > div.panel-heading.accordion-toggle.collapsed";
    await $(element).waitForExist();
    await browser.pause(2000);
    await $(element).click();

    // get upper sacramento water conditions
    element = "#headingEight > div > div:nth-child(2) > .label-default-danger";
    await $(element).waitForExist();
    await browser.pause(2000);
    let upperSacramentoConditions = await $(element).getText();
    element = "#headingFour  .label-default-danger";
    let lowerSacramentoConditions = await $(element).getText();

    // update spots
    const spotsToUpdateKeys = [
      "ca-upper-sac-dog-creek",
      "ca-upper-sac-tauhindauli-park-and-trail",
      "ca-upper-sac-Financial-Avenue",
      "ca-upper-sac-soda-creek",
      "ca-upper-sac-Castle-Crags-State-Park",
      "ca-upper-sac-Sweetbrier",
      "ca-upper-sac-Coant",
      "ca-upper-sac-Sims",
      "ca-upper-sac-Gibson",
      "ca-upper-sac-Pollard-Gulch",
      "ca-upper-sac-Lamoine",
      "ca-upper-sac-McCardle-Flat",
      "ca-upper-sac-scarlett-way",
      "ca-upper-sac-prospect-avenue",
    ];

    if (upperSacramentoConditions) {
      for (
        spotCounter = 0;
        spotCounter < spotsToUpdateKeys.length;
        spotCounter++
      ) {
        console.log(
          `${getTimeStamp()}: Updating ${spotsToUpdateKeys[spotCounter]} with condition: ${upperSacramentoConditions}..........\n`
        );

        try {
          const spotsSearchResults = await client.query({
            query: SPOTS_QUERY,
            variables: {
              searchTerm: spotsToUpdateKeys[spotCounter],
            },
          });

          const dbSpot = spotsSearchResults.data.searchSpots.find((spot) => {
            return spot.key === spotsToUpdateKeys[spotCounter];
          });

          const variables = {
            spotId: dbSpot.id,
            data: JSON.stringify({ condition: upperSacramentoConditions }),
          };

          const updateSpotRequest = await client.mutate({
            mutation: UPDATE_SPOT_MUTATION,
            variables,
          });
        } catch (e) {
          console.log(e);
        }
      }
    }

    // get lower sacramento river conditions
    const lowerSacUpdateKeys = [
      "ca-lower-sac-redding-civic-auditorium",
      "ca-lower-sac-tbedp",
    ];

    if (lowerSacramentoConditions) {
      for (
        spotCounter = 0;
        spotCounter < lowerSacUpdateKeys.length;
        spotCounter++
      ) {
        console.log(
          `${getTimeStamp()}: Updating ${lowerSacUpdateKeys[spotCounter]} with condition: ${lowerSacramentoConditions}..........\n`
        );


        try {
          const spotsSearchResults = await client.query({
            query: SPOTS_QUERY,
            variables: {
              searchTerm: lowerSacUpdateKeys[spotCounter],
            },
          });

          const dbSpot = spotsSearchResults.data.searchSpots.find((spot) => {
            return spot.key === lowerSacUpdateKeys[spotCounter];
          });

          const variables = {
            spotId: dbSpot.id,
            data: JSON.stringify({ condition: lowerSacramentoConditions }),
          };

          const updateSpotRequest = await client.mutate({
            mutation: UPDATE_SPOT_MUTATION,
            variables,
          });
        } catch (e) {
          console.log(e);
        }
      }
    }
  }).timeout(99999999999999999999);
});
